import OrderReducer from './Reducers/OrderReducer';
import TransactionReducer from'./Reducers/TransactionReducer';
import accountReducer from './Reducers/accountReducer';
import sessionReducer from './Reducers/sessionReducer';

const {combineReducers} = require('redux');
const {default: StoreReducer} = require('./Reducers/StoreReducer');

const rootReducer = combineReducers({
  Store: StoreReducer,
  Order: OrderReducer,
  Transaction: TransactionReducer,
  account: accountReducer,
  session: sessionReducer,

});

export default rootReducer;
