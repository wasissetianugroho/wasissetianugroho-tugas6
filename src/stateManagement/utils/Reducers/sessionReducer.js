const initialSessionState = {
    session: {
      email: '',
      nama: '',
    },
  };
  
  const sessionReducer = (state = initialSessionState, action) => {
    console.log('session reducer: ', action);
    switch (action.type) {
      case 'SESSION_ADD_DATA':
        return {
          ...state,
          session: action.data,
        };
  
      case 'SESSION_DELETE_DATA':
        return {
          ...state,
          session: {
            email: '',
            nama: '',
          },
        };
  
      default:
        return state;
    }
  };
  
  export default sessionReducer;  