const initialTransactionState = {
  trans: [],
};

// id_trans, date, product: [id_sepatu, merk, warna, ukuran, [service], note, toko: [data toko]], paid

const TransactionReducer = (state = initialTransactionState, action) => {
  console.log('transaction reducer: ', action);
  switch (action.type) {
    case 'TRANS_ADD_DATA':
      return {
        ...state,
        trans: action.data,
      };

    case 'TRANS_UPDATE_DATA':
      var newData = [...state.trans];
      var findIndex = newData.findIndex(
        value => value.id_trans === action.data.id_trans,
      );
      newData[findIndex] = action.data;
      return {
        ...state,
        trans: newData,
      };

    default:
      return state;
  }
};

export default TransactionReducer;