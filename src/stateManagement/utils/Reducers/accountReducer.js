const initialAccountState = {
    account: [],
  };
  
  const accountReducer = (state = initialAccountState, action) => {
    console.log('auth reducer: ', action);
    switch (action.type) {
      case 'AUTH_ADD_DATA':
        return {
          ...state,
          account: action.data,
        };
  
      default:
        return state;
    }
  };
  
  export default accountReducer;  