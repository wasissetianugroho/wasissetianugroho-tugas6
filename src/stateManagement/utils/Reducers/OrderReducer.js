const initialOrderState = {
  product: [],
};

// id_sepatu, merk, warna, ukuran, [service], note, store

const OrderReducer = (state = initialOrderState, action) => {
  console.log('order reducer: ', action);
  switch (action.type) {
    case 'ORDER_ADD_DATA':
      return {
        ...state,
        product: action.data,
      };

    case 'ORDER_DELETE_DATA':
      var newData = [...state.product];
      var findIndex = state.product.findIndex(value => {
        return value.id_sepatu === action.data.id_sepatu;
      });
      console.log('index: ', findIndex);
      newData.splice(findIndex, 1);
      return {
        ...state,
        product: newData,
      };

    default:
      return state;
  }
};

export default OrderReducer;
  