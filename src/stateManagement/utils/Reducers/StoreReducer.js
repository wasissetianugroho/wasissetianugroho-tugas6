const storeDummy = [
    {
      id: 1,
      nama: 'Jack Repair Gejayan',
      alamat: 'Jalan Affandi (Gejayan), No.20, Sleman Yogyakarta, 55384',
      buka: false,
      rating: 4.8,
      suka: true,
      jadwal: '08:00 - 20:00',
      deskripsi:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.',
      biaya: 'Rp 15.000 - 65.000',
      image: require('../../../assets/Foto1.png'),
    },
    {
      id: 2,
      nama: 'Jack Repair Seturan',
      alamat: 'Jalan Seturan, No.15, Depok, Sleman Yogyakarta, 55384',
      buka: true,
      rating: 4.7,
      suka: false,
      jadwal: '09:00 - 21:00',
      deskripsi:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.',
      biaya: 'Rp 20.000 - 80.000',
      image: require('../../../assets/Foto2.png'),
    },
  ];
  
  const initialStoreState = {
    stores: storeDummy,
  };
  
  const StoreReducer = (state = initialStoreState, action) => {
    console.log('store reducer: ', action);
    switch (action.type) {
      case 'STORE_ADD_DATA':
        return {
          ...state,
          stores: action.data,
        };
  
      default:
        return state;
    }
  };
  
  export default StoreReducer;  