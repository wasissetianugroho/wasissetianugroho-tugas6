import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  // COMMON STYLE ===================================================================
  safeAreaContent: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  textTitle: {
    fontSize: 25,
    fontWeight: '900',
  },
  textSubTitle: {
    fontSize: 16,
  },
  textDesc: {
    fontSize: 15,
  },
  profileImageSmall: {
    width: 50,
    height: 55,
  },
  iconSmall: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  // HOME STYLE =====================================================================

  // TOP CONTENT ====================================================================
  topContent: {
    backgroundColor: 'white',
    paddingHorizontal: 22,
    paddingTop: 56,
    paddingBottom: 28,
  },
  rowSpaceBetween: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textInputImageForm: {
    backgroundColor: '#F6F8FF',
    flexDirection: 'row',
    flex: 1,
    paddingRight: 12,
    borderRadius: 12,
  },
  imageButton: {
    backgroundColor: '#F6F8FF',
    alignSelf: 'baseline',
    alignSelf: 'center',
    paddingVertical: 4,
    marginLeft: 15,
    borderRadius: 12,
  },
  // CATEGORY CONTENT ===============================================================
  categoryContent: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  categoryButton: {
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 16,
    borderRadius: 12,
    marginTop: 16,
  },
  categoryImage: {
    resizeMode: 'contain',
    height: 50,
    width: 55,
    marginBottom: 9,
  },
  // LIST ITEM CONTENT ==============================================================
  itemContent: {
    marginTop: 27,
    paddingHorizontal: 22,
  },
  cardContent: {
    borderRadius: 12,
    marginBottom: 8,
    padding: 8,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  cardImage: {
    width: 80,
    height: 121,
    marginRight: 16,
    resizeMode: 'contain',
  },
  textLocationName: {
    fontSize: 20,
    fontWeight: '900',
    color: 'black',
  },
  textLocationDesc: {
    color: '#D8D8D8',
    fontSize: 11,
  },
  textClosed: {
    backgroundColor: '#E64C3C33',
    color: '#EA3D3D',
    fontWeight: '700',
    padding: 8,
    alignSelf: 'baseline',
    borderRadius: 24,
    marginTop: 20,
  },
  textOpen: {
    backgroundColor: '#11A84E1F',
    color: '#11A84E',
    fontWeight: '700',
    padding: 10,
    alignSelf: 'baseline',
    borderRadius: 25,
    marginTop: 15,
  },
});

export {styles};