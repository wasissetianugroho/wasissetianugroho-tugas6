import {StyleSheet} from 'react-native';

const textStyle = StyleSheet.create({
  size13BlackBold: {
    fontSize: 13,
    color: 'black',
    fontWeight: '900',
  },
  size15BlackBold: {
    fontSize: 15, 
    fontWeight: '900',
  },
  size16BlackBold: {
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold',
  },
  size25BlackBold: {
    fontSize: 25,
    color: 'black',
    fontWeight: '900',
  },
  size25GreenBold: {
    fontSize: 25,
    fontWeight: '700',
  },
  size36BlackBold: {
    fontSize: 36,
    color: 'black',
    fontWeight: 'bold',
  },
  size15BlackNormal: {
    fontSize: 15,
    fontWeight: 'normal',
    color: 'black',
  },
  size25BlackNormal: {
    fontSize: 25,
    color: 'black',
    fontWeight: 'normal',
  },
  size16BlackNormal: {
    fontSize: 16,
    fontWeight: 'normal',
    color: 'black',
  },
  size18BlackNormal: {
    fontSize: 18,
    fontWeight: 'normal',
    color: 'black',
  },
});

export {textStyle};
