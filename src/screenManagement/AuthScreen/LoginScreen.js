import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';
import {ScrollView} from 'react-native-gesture-handler';
import {styles} from '../../assets/style/AuthStyles';
import { useState } from 'react';
import { Base64 } from 'js-base64';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';

const LoginScreen = ({navigation}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const {account} = useSelector(state => state.account);
    const {session} = useSelector(state => state.session);
    const dispatch = useDispatch();
  
    useEffect(() => {
      if (session.nama && session.email) {
        console.log('ada session');
        navigation.replace('MainRouting');
      } else {
        console.log('tdk ada session');
      }
    });
  
    const loginSession = () => {
      const emailValidation = validate(email);
      const encrypedtPassword = Base64.encode(password);
  
      if (emailValidation && password) {
        if (checkAccount(email, encrypedtPassword)) {
          navigation.replace('MainRouting');
        }
      } else {
        ToastAndroid.show(
          'Login gagal, cek email dan password!',
          ToastAndroid.SHORT,
        );
      }
    };
  
    const checkAccount = (email, encrypedtPassword) => {
      var acc = account.find(value => value.email === email);
  
      console.log('all account: ', account);
      console.log('check login account: ', acc);
      if (acc === undefined) {
        ToastAndroid.show(
          'Akun tidak ada, silahkan daftar terlebih dahulu!',
          ToastAndroid.SHORT,
        );
        return false;
      } else if (acc.password == encrypedtPassword) {
        dispatch({
          type: 'SESSION_ADD_DATA',
          data: {email: acc.email, nama: acc.nama},
        });
        ToastAndroid.show(
          `Login berhasil, selamat datang ${acc.nama}!`,
          ToastAndroid.SHORT,
        );
        return true;
      } else {
        ToastAndroid.show(
          `Login gagal, cek email dan password!`,
          ToastAndroid.SHORT,
        );
        return false;
      }
    };
  
    const validate = text => {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
      if (reg.test(text) === false) {
        console.log('Email is Not Correct');
        return false;
      } else {
        console.log('Email is Correct');
        return true;
      }
    };  
  return (
    <View style={styles.safeAreaContent}>
      {/* MAIN CONTENT ============================================================================ */}
      <ScrollView style={styles.scrollViewContent}>
        <Image
          style={styles.splashImage}
          source={require('../../assets/Smile.png')}
        />

        {/* FORM */}
        <View style={styles.formContent}>
          <Text style={styles.titleText}>Welcome,{'\n'}Please Login First</Text>

          <Text style={[styles.labelFormText, {marginTop: 25}]}>Email</Text>
          <TextInput
            style={styles.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="youremail@gmail.com"
            keyboardType="email-address"
          />

          <Text style={[styles.labelFormText, {marginTop: 8}]}>Password</Text>
          <TextInput
            style={styles.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Password***"
            secureTextEntry={true}
          />

          {/* LOGIN method ============================= */}
          <View style={styles.loginMethodContent}>
            <Image
              style={{marginRight: 18}}
              source={require('../../assets/Gmail.png')}
            />
            <Image
              style={{marginRight: 18}}
              source={require('../../assets/Facebook.png')}
            />
            <Image
              style={{marginRight: 18}}
              source={require('../../assets/Twitter.png')}
            />
            <Text
              style={{
                alignSelf: 'center',
                marginLeft: 'auto',
                color: '#717171',
              }}>
              Forgort Password?
            </Text>
          </View>

          {/* BUTTON ============================================*/}
          <TouchableOpacity
            onPress={() => navigation.navigate('MainRouting')}
            style={styles.buttonStyle}>
            <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
              Login
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>

      {/* FOOTER ================================================================================= */}
      <View style={styles.footerContent}>
        <Text>
          Don't Have An Account?{' '}
          <Text
            onPress={() => {
              navigation.navigate('RegisterScreen');
            }}
            style={{fontWeight: '500', color: '#BB2427'}}>
            Register
          </Text>
        </Text>
      </View>
    </View>
  );
};

export default LoginScreen;