import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import React, {Component, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {styles} from '../../assets/style/HomeStyle';
import {textStyle} from '../../assets/style/TextStyle';
import moment from 'moment/moment';
import uuid from 'react-native-uuid';

import {useDispatch, useSelector} from 'react-redux';

const SummaryScreen = ({navigation, route}) => {
  useEffect(() => {
    var date = moment().utcOffset('+05:30').format('dddd, D MMMM YYYY');
    console.log('route params', route.params.id_sepatu);
    console.log('date ', date);
  });
  const data = route.params;
  const {trans} = useSelector(state => state.Transaction);
  const {product} = useSelector(state => state.Order);
  const dispatch = useDispatch();

  const deleteOrder = () => {
    const deleteOrderData = {...data};
    console.log('deleteOrderData : ', deleteOrderData);
    dispatch({type: 'ORDER_DELETE_DATA', data: deleteOrderData});
  };

  const addTransaction = () => {
    var date = moment().utcOffset('+05:30').format('dddd, D MMMM YYYY');
    var newData = [...trans];
    const newTrans = {
      id_trans: uuid.v4(),
      date: date,
      product: data,
      paid: false,
    };
    newData.push(newTrans);
    dispatch({type: 'TRANS_ADD_DATA', data: newData});
    deleteOrder();
    navigation.navigate('ReservasiSuksesScreen');
  };
  return (
    <View style={styles.safeAreaContent}>
      <ScrollView style={{flexGrow: 1}}>
        {/* Data Customer ================================================================== */}
        <View
          style={{
            paddingHorizontal: 24,
            paddingVertical: 18,
            marginTop: 12,
            backgroundColor: 'white',
          }}>
          <Text style={[textStyle.size15BlackNormal, {color: '#979797'}]}>
            Data Customer
          </Text>
          <Text style={[textStyle.size15BlackNormal, {marginTop: 10}]}>
            Wasis Setia Nugroho (081393530013)
          </Text>
          <Text style={[textStyle.size15BlackNormal, {marginTop: 8}]}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={[textStyle.size15BlackNormal, {marginTop: 10}]}>
            gantengdoang@dipanggang.com
          </Text>
        </View>

        {/* ALAMAT TUJUAN ================================================================= */}
        <View
          style={{
            paddingHorizontal: 24,
            paddingVertical: 18,
            marginTop: 12,
            backgroundColor: 'white',
          }}>
          <Text style={[textStyle.size15BlackNormal, {color: '#979797'}]}>
            Alamat Outlet Tujuan
          </Text>
          <Text style={[textStyle.size15BlackNormal, {marginTop: 10}]}>
            {data.store.nama} (027-343457)
          </Text>
          <Text style={[textStyle.size15BlackNormal, {marginTop: 8}]}>
            {data.store.alamat}
          </Text>
        </View>

        {/* BARANG =========================================================================== */}
        <View
          style={{
            paddingHorizontal: 24,
            paddingVertical: 18,
            marginTop: 12,
            backgroundColor: 'white',
          }}>
          <Text style={[textStyle.size15BlackNormal, {color: '#979797'}]}>
            Barang
          </Text>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <Image
              source={require('../../assets/SepatuNB.png')}
              style={localStyle.cardImage}
            />
            <View style={{marginHorizontal: 13, justifyContent: 'center'}}>
              <Text style={textStyle.size15BlackBold}>
                {data.merek} - {data.warna} - {data.ukuran}
              </Text>
              <Text
                style={[
                  textStyle.size15BlackNormal,
                  {color: '#737373', width: '80%'},
                ]}>
                {data.service.map(text => text.key).join(', ')}
              </Text>
              <Text style={[textStyle.size15BlackNormal, {color: '#737373'}]}>
                Note : {data.note}
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
      {/* FOOTER ====================================================================== */}
      <View style={localStyle.footerContent}>
        <TouchableOpacity
          onPress={() => addTransaction()}
          style={localStyle.buttonStyle}>
          <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
            Reservasi Sekarang
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const localStyle = StyleSheet.create({
  buttonStyle: {
    backgroundColor: '#BB2427',
    alignItems: 'center',
    padding: 19,
    borderRadius: 8,
  },
  cardImage: {
    width: 84,
    height: 84,
    resizeMode: 'contain',
    borderRadius: 8,
  },
  footerContent: {
    bottom: 0,
    marginBottom: 48,
    paddingHorizontal: 20,
    width: '100%',
    position: 'absolute',
    alignSelf: 'center',
  },
});

export default SummaryScreen;