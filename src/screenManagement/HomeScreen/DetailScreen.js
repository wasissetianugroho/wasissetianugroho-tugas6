import {
    Image,
    ImageBackground,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React from 'react';
  import {styles} from '../../assets/style/HomeStyle';
  import {textStyle} from '../../assets/style/TextStyle';
  
  const DetailScreen = ({navigation, route}) => {
    const storeData = route.params;
    return (
      <View style={[styles.safeAreaContent]}>
        <ScrollView style={{flexGrow: 1}}>
          {/* Location Image ============================================================ */}
          <ImageBackground
            style={detailStyle.imageLocation}
            source={storeData.image}>
            <View style={detailStyle.detailAppbar}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Image
                  style={[detailStyle.imageSize24, {tintColor: 'white'}]}
                  source={require('../../assets/Back.png')}
                />
              </TouchableOpacity>
              <Image
                style={[detailStyle.imageSize24,]}
                source={require('../../assets/TasPutih.png')}
              />
            </View>
          </ImageBackground>
  
          {/* MAIN CONTENT ========================================================== */}
          <View style={detailStyle.mainContent}>
            {/* TITLE CONTENT */}
            <View style={{marginHorizontal: 24}}>
              <Text style={textStyle.size25BlackBold}>{storeData.nama}</Text>
              <Image
                style={[detailStyle.starSize65]}
                source={require('../../assets/Bintang.png')}
              />
              {/* Alamat ===================================================*/}
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 12,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                  }}>
                  <Image
                    style={[detailStyle.imageSize24]}
                    source={require('../../assets/Gmaps.png')}
                  />
                  <Text style={[textStyle.size15BlackNormal, {color: '#979797'}]}>
                    {storeData.alamat}
                  </Text>
                </View>
                <Text
                  style={[
                    textStyle.size12BlackBold,
                    {
                      color: '#3471CD',
                      marginLeft: 'auto',
                      height: '100%',
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  Lihat Maps
                </Text>
              </View>
  
              {/* Buka dan Tutup  ==================================*/}
              <View
                style={{
                  marginTop: 12,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={
                    storeData.buka ? detailStyle.textOpen : detailStyle.textClosed
                  }>
                  {storeData.buka ? 'BUKA' : 'TUTUP'}
                </Text>
                <Text style={[textStyle.size13BlackBold, {marginLeft: 16}]}>
                  {storeData.jadwal}
                </Text>
              </View>
            </View>
  
            {/* Garis ===================================*/}
            <View
              style={{
                marginTop: 13,
                borderBottomColor: '#EEEEEE',
                borderBottomWidth: StyleSheet.hairlineWidth,
              }}
            />
            {/* Diskripsi ===============================*/}
            <View style={{marginHorizontal: 24, marginTop: 24}}>
              <Text style={textStyle.size16BlackNormal}>Description</Text>
              <Text
                style={[
                  textStyle.size16BlackNormal,
                  {color: '#595959', textAlign: 'justify', marginTop: 10},
                ]}>
                {storeData.deskripsi}
              </Text>
  
              <Text style={[textStyle.size16BlackNormal, {marginTop: 24}]}>
                Range Biaya
              </Text>
              <Text
                style={[
                  textStyle.size16BlackNormal,
                  {color: '#8D8D8D', marginTop: 8},
                ]}>
                {storeData.biaya}
              </Text>
              {storeData.buka && (
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('PemesananScreen', storeData)
                  }
                  style={detailStyle.buttonStyle}>
                  <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
                    Repair Disini
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };
  
  const detailStyle = StyleSheet.create({
    imageLocation: {
      width: '100%',
      height: 316,
      resizeMode: 'contain',
    },
    detailAppbar: {
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: 24,
      paddingVertical: 22,
    },
    imageSize24: {
      width: 24,
      height: 24,
      resizeMode: 'contain',
    },
    starSize65: {
      width: 65,
      height: 12,
      resizeMode: 'stretch',
    },
    mainContent: {
      flex: 1,
      borderTopLeftRadius: 16,
      borderTopRightRadius: 16,
      marginTop: -16,
      paddingVertical: 24,
      backgroundColor: 'white',
    },
    textOpen: {
      backgroundColor: '#11A84E1F',
      color: '#11A84E',
      fontWeight: '700',
      padding: 8,
      alignSelf: 'baseline',
      borderRadius: 24,
    },
    textClosed: {
      backgroundColor: '#E64C3C33',
      color: '#EA3D3D',
      fontWeight: '700',
      padding: 8,
      alignSelf: 'baseline',
      borderRadius: 24,
    },
    buttonStyle: {
      backgroundColor: '#BB2427',
      marginTop: 35,
      alignItems: 'center',
      padding: 19,
      borderRadius: 8,
    },
  });
  
  export default DetailScreen;  