import {
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React from 'react';
  import {styles} from '../../assets/style/HomeStyle';
  import {textStyle} from '../../assets/style/TextStyle';
  
  const ReservasiSuksesScreen = ({navigation}) => {
    return (
      <View style={[styles.safeAreaContent, {backgroundColor: 'white'}]}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('MainRouting', {
                screen: 'Transaction',
              })
            }
            style={{alignSelf: 'baseline', margin: 24, position: 'absolute'}}>
            <Image
              style={{
                width: 24,
                height: 24,
              }}
              source={require('../../assets/Close.png')}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={[textStyle.size25GreenBold, {color: '#11A84E'}]}>
              Reservasi Berhasil
            </Text>
            <Image
              style={{width: 220, height: 220, marginVertical: 80}}
              source={require('../../assets/CentangHijau.png')}
            />
            <Text style={([textStyle.size25BlackNormal], {textAlign: 'center'})}>
              Kami Telah Mengirimkan Kode
            </Text>
            <Text style={([textStyle.size25BlackNormal], {textAlign: 'center'})}>
              Reservasi Ke Menu Transaksi
            </Text>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('MainRouting', {
                  screen: 'Transaction',
                })
              }
              style={localStyle.buttonStyle}>
              <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
                Lihat Kode Reservasi
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  };
  
  const localStyle = StyleSheet.create({
    buttonStyle: {
      backgroundColor: '#BB2427',
      alignItems: 'center',
      marginTop: 50,
      padding: 20,
      borderRadius: 8,
    },
    footerContent: {
      bottom: 0,
      marginBottom: 48,
      paddingHorizontal: 20,
      width: '100%',
      position: 'absolute',
      alignSelf: 'center',
    },
  });
  
  export default ReservasiSuksesScreen;  