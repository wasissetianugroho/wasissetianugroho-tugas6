import {
    FlatList,
    Image,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    StyleSheet,
  } from 'react-native';
  import React from 'react';
  import {styles} from '../../assets/style/HomeStyle';
  import {useSelector} from 'react-redux';
  
  const HomeScreen = ({navigation, route}) => {
    const {stores} = useSelector(state => state.Store);
    console.log('home route: ', route);
    console.log('store data: ', stores);
    return (
      <View style={styles.safeAreaContent}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
          {/* TOP CONTENT =================================================================== */}
          <View style={styles.topContent}>
            <View style={styles.rowSpaceBetween}>
              <Image
                style={styles.profileImageSmall}
                source={require('../../assets/Wasis.jpg')}
              />
               <TouchableOpacity
              onPress={() =>
                navigation.navigate('HomeRouting', {screen: 'KeranjangScreen'})
              }>
              <Image
                style={[styles.iconSmall, {height: 20}, {tintColor: 'black'}]}
                source={require('../../assets/TasPutih.png')}
              />
              </TouchableOpacity>
            </View>
            <Text
              style={[
                styles.textSubTitle,
                {
                  color: '#034262',
                  alignSelf: 'baseline',
                  paddingVertical: 10,
                },
              ]}>
              Hello, Wasis!
            </Text>
            <Text style={[styles.textTitle, {color: '#0A0827'}]}>
              Ingin merawat dan memperbaiki sepatumu? cari disini
            </Text>
  
            <View style={{flexDirection: 'row', marginTop: 20}}>
              <View style={[styles.textInputImageForm]}>
                <Image
                  style={[styles.iconSmall, {margin: 12}]}
                  source={require('../../assets/Search.png')}
                />
                <TextInput style={{flex: 1}} />
              </View>
  
              <TouchableOpacity style={styles.imageButton}>
                <Image
                  style={[styles.iconSmall, {margin: 12}]}
                  source={require('../../assets/Filter.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* CATEGORY CONTENT ============================================================== */}
          <View style={styles.categoryContent}>
            <TouchableOpacity style={styles.categoryButton}>
              <Image
                style={styles.categoryImage}
                source={require('../../assets/Sepatu.png')}
              />
              <Text>Sepatu</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.categoryButton}>
              <Image
                style={styles.categoryImage}
                source={require('../../assets/Jaket.png')}
              />
              <Text>Jaket</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.categoryButton}>
              <Image
                style={styles.categoryImage}
                source={require('../../assets/Ransel.png')}
              />
              <Text>Tas</Text>
            </TouchableOpacity>
          </View>
  
          {/* LIST ITEM CONTENT ============================================================= */}
          <View style={styles.itemContent}>
            <View style={[styles.rowSpaceBetween, {marginBottom: 24}]}>
              <Text
                style={[styles.textDesc, {fontWeight: '600', color: 'black'}]}>
                Rekomendasi Terdekat
              </Text>
              <Text style={[{fontSize: 12, fontWeight: '600', color: '#E64C3C'}]}>
                View All
              </Text>
            </View>
  
            <FlatList
              data={stores}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('HomeRouting', {
                      screen: 'DetailScreen',
                      params: item,
                    })
                  }>
                  <View style={styles.cardContent}>
                    <Image style={styles.cardImage} source={item.image} />
                    <View style={{flex: 1}}>
                      <View style={styles.rowSpaceBetween}>
                        <View>
                          <Image
                            style={{width: 50, height: 8, resizeMode: 'contain'}}
                            source={require('../../assets/Bintang.png')}
                          />
                          <Text style={{color: '#D8D8D8'}}>
                            {item.rating} Ratings
                          </Text>
                        </View>
                        <Image
                          style={{
                            width: 12,
                            height: 13,
                            resizeMode: 'contain',
                            alignSelf: 'flex-start',
                          }}
                          source={
                            item.suka
                              ? require('../../assets/Love1.png')
                              : require('../../assets/Love2.png')
                          }
                        />
                      </View>
                      <Text style={styles.textLocationName}>{item.nama}</Text>
                      <Text style={styles.textLocationDesc}>{item.alamat}</Text>
                      <Text
                        style={item.buka ? styles.textOpen : styles.textClosed}>
                        {item.buka ? 'BUKA' : 'TUTUP'}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </ScrollView>
      </View>
    );
  };
  
export default HomeScreen;  