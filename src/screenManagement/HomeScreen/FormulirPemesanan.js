import {
    FlatList,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React, {useEffect, useState} from 'react';
  import {styles} from '../../assets/style/HomeStyle';
  import CheckBox from '@react-native-community/checkbox';
  import uuid from 'react-native-uuid';
  import {useDispatch, useSelector} from 'react-redux';
  
  const FormulirPemesanan = ({navigation, route}) => {
    useEffect(() => {
    console.log('Form Store Data: ', route.params);
    });
    const dispatch = useDispatch();
    const {product} = useSelector(state => state.Order);
    const Store = route.params;

    const [merek, setMerek] = useState('');
    const [warna, setWarna] = useState('');
    const [ukuran, setUkuran] = useState('');
    const [note, setNote] = useState('');
    const [data, setData] = useState([
      {key: 'Ganti Sol Sepatu', checked: false},
      {key: 'Jahit Sepatu', checked: false},
      {key: 'Repaint Sepatu', checked: false},
      {key: 'Cuci Sepatu', checked: false},
    ]);

    const addData = () => {
      var newData = [...product];
      var filteredService = data.filter(datum => {
        return datum.checked === true;
      });  

    console.log('merek: ', merek ? merek : 'tidak ada');
    console.log(
      'service: ',
      filteredService.length > 0 ? filteredService : 'tidak ada',
    );

    if (merek && warna && ukuran && note && filteredService.length > 0) {
      const orderData = {
        id_sepatu: uuid.v4(),
        merek: merek,
        warna: warna,
        ukuran: ukuran,
        service: filteredService,
        note: note,
        store: Store,
      };
      newData.push(orderData);
      dispatch({type: 'ORDER_ADD_DATA', data: newData});
      navigation.navigate('KeranjangScreen');
    } else {
      console.log('isi data');
    }
  };
  const handleItemPress = item => {
      const updatedData = data.map(x =>
        x.key === item.key ? {...x, checked: !x.checked} : x,
      );
      setData(updatedData);
    };
  
    return (
      <View style={[styles.safeAreaContent, {backgroundColor: 'white'}]}>
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
            marginHorizontal: 20,
            marginVertical: 24,
          }}>
          <Text style={[localStyle.labelFormText, {marginTop: 8}]}>Merek</Text>
          <TextInput
            onChangeText={text => setMerek(text)}
            value={merek}
            style={localStyle.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Masukkan Merk Barang"
          />
          <Text style={[localStyle.labelFormText, {marginTop: 24}]}>Warna</Text>
          <TextInput
            onChangeText={text => setWarna(text)}
            value={warna}  
            style={localStyle.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Warna Barang, cth : Merah - Putih "
          />
          <Text style={[localStyle.labelFormText, {marginTop: 24}]}>Ukuran</Text>
          <TextInput
            onChangeText={text => setUkuran(text)}
            value={ukuran}  
            style={localStyle.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Cth : S, M, L / 39,40"
            keyboardType="numeric"
          />
          <Text style={[localStyle.labelFormText, {marginTop: 24}]}>Photo</Text>
          <TouchableOpacity style={localStyle.photoInput}>
            <Image
              style={{width: 23, height: 18, marginBottom: 12}}
              source={require('../../assets/Camera.png')}
            />
            <Text>Add Photo</Text>
          </TouchableOpacity>
  
          <FlatList
            style={{marginTop: 32}}
            // data={dataCheckBox}
            data={data}
            renderItem={({item}) => (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginVertical: 4,
                }}>
                <CheckBox
                  value={item.checked}
                  onValueChange={() => handleItemPress(item)}
                />
                <Text style={styles.item}>{item.key}</Text>
              </View>
            )}
          />
          
          <Text style={[localStyle.labelFormText, {marginTop: 24}]}>Catatan</Text>
          <TextInput
          onChangeText={text => setNote(text)}
          value={note}
            numberOfLines={4}
            multiline
            maxLength={40}
            style={[localStyle.textInputStyle, {textAlignVertical: 'top'}]}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Cth : ingin ganti sol baru "
          />
          <TouchableOpacity
            onPress={() => {addData();}}
            style={localStyle.buttonStyle}>
            <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
              Masukkan Keranjang
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  };
  
  const localStyle = StyleSheet.create({
    textInputStyle: {
      backgroundColor: '#F6F8FF',
      borderRadius: 7,
      paddingHorizontal: 11,
      paddingVertical: 14,
      marginTop: 11,
    },
    labelFormText: {
      color: '#BB2427',
      fontSize: 12,
      fontWeight: '600',
    },
    photoInput: {
      alignSelf: 'baseline',
      alignItems: 'center',
      borderColor: '#BB2427',
      width: 84,
      borderWidth: 1,
      borderRadius: 8,
      marginTop: 24,
      paddingTop: 24,
      paddingBottom: 12,
      paddingHorizontal: 8,
    },
    buttonStyle: {
      backgroundColor: '#BB2427',
      marginTop: 35,
      marginBottom: 40,
      alignItems: 'center',
      padding: 19,
      borderRadius: 8,
    },
  });
  
  export default FormulirPemesanan;  