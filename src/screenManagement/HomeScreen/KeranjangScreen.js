import {
    FlatList,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React, {useEffect} from 'react';
  import {styles} from '../../assets/style/HomeStyle';
  import {textStyle} from '../../assets/style/TextStyle';
  import {useSelector} from 'react-redux';
  
  const KeranjangScreen = ({navigation, route}) => {
    const {product} = useSelector(state => state.Order);
    useEffect(() => {
      console.log('parameter : ', product);
    });
  
    return (
      <View style={styles.safeAreaContent}>
        {/* MAIN CONTENT ====================================================================== */}
        <ScrollView style={{flexGrow: 1}}>
          {/* Card view ========================================================================*/}
          <FlatList
            data={product}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <TouchableOpacity
              // onPress={() => navigation.navigate('Edit', item)}
                  onPress={() => navigation.navigate('SummaryScreen', item)}
              style={localStyle.cardView}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    source={require('../../assets/SepatuNB.png')}
                    style={localStyle.cardImage}
                  />
                  <View style={{marginHorizontal: 13, justifyContent: 'center'}}>
                    <Text style={textStyle.size15BlackBold}>
                      {item.merek} - {item.warna} - {item.ukuran}
                    </Text>
                    <Text
                      style={[
                        textStyle.size15BlackNormal,
                        {color: '#737373', width: '80%'},
                      ]}>
                      {item.service.map(text => text.key).join(', ')}
                    </Text>
                    <Text
                      style={[textStyle.size15BlackNormal, {color: '#737373'}]}>
                      Note : {item.note}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
          {/* Tambah keranjang ======================================================*/}
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 43,
              marginBottom: 180,
            }}>
            <Image
              style={localStyle.iconStyle}
              source={require('../../assets/Plus.png')}
            />
            <Text
              style={[
                textStyle.size15BlackBold,
                {color: '#BB2427', marginLeft: 8},
              ]}>
              Tambah Keranjang
            </Text>
          </TouchableOpacity>
        </ScrollView>
        {/* FOOTER ===================================================================================== */}
        <View style={localStyle.footerContent}>
          <TouchableOpacity
            onPress={() => navigation.navigate('SummaryScreen')}
            style={localStyle.buttonStyle}>
            <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
              Selanjutnya
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  
  const localStyle = StyleSheet.create({
    cardView: {
      backgroundColor: 'white',
      paddingHorizontal: 14,
      paddingVertical: 24,
      borderRadius: 8,
      elevation: 4,
      margin: 11,
    },
    cardImage: {
      width: 84,
      height: 84,
      resizeMode: 'contain',
      borderRadius: 8,
    },
    buttonStyle: {
      backgroundColor: '#BB2427',
      alignItems: 'center',
      padding: 19,
      borderRadius: 8,
    },
    iconStyle: {
      width: 20,
      height: 20,
    },
    footerContent: {
      bottom: 0,
      paddingBottom: 40,
      paddingTop: 20,
      backgroundColor: 'white',
      paddingHorizontal: 20,
      width: '100%',
      position: 'absolute',
      alignSelf: 'center',
    },
  });
  export default KeranjangScreen;  