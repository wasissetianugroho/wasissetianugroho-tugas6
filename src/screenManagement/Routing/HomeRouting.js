import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../HomeScreen/HomeScreen';
import DetailScreen from '../HomeScreen/DetailScreen';
import FormulirPemesanan from '../HomeScreen/FormulirPemesanan';
import KeranjangScreen from '../HomeScreen/KeranjangScreen';
import SummaryScreen from '../HomeScreen/SummaryScreen';
import ReservasiSuksesScreen from '../HomeScreen/ReservasiSuksesScreen';

const Stack = createStackNavigator();
const HomeRouting = ({navigation}) => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="HomeScreen">
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="DetailScreen" component={DetailScreen} />
      <Stack.Screen
        name="PemesananScreen"
        component={FormulirPemesanan}
        options={{headerShown: true, title: 'FormulirPemesanan'}}
      />
      <Stack.Screen
        name="KeranjangScreen"
        component={KeranjangScreen}
        options={{headerShown: true, title: 'Keranjang'}}
      />
      <Stack.Screen
        name="SummaryScreen"
        component={SummaryScreen}
        options={{headerShown: true, title: 'Summary'}}
      />
      <Stack.Screen
        name="ReservasiSuksesScreen"
        component={ReservasiSuksesScreen}
      />
    </Stack.Navigator>
  );
};

export default HomeRouting;