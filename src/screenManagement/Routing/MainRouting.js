import React from 'react';
import MyTabBar from '../../assets/MyTabBar';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeRouting from './HomeRouting';
import TransactionRouting from './TransactionRouting';
import ProfileRouting from './ProfileRouting';

const Tab = createBottomTabNavigator();
const MainRouting = () => {
  return (
    <Tab.Navigator
      tabBar={props => <MyTabBar {...props} />}
      screenOptions={{headerShown: false}}
      initialRouteName="Home">
      <Tab.Screen name="Home" component={HomeRouting} />
      <Tab.Screen name="Transaction" component={TransactionRouting} />
      <Tab.Screen name="Profile" component={ProfileRouting} />
    </Tab.Navigator>
  );
};

export default MainRouting;