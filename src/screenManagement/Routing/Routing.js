import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginScreen from '../AuthScreen/LoginScreen';
import RegisterScreen from '../AuthScreen/RegisterScreen';
import SplashScreen from '../AuthScreen/SplashScreen';
import MainRouting from '../Routing/MainRouting';
import HomeRouting from '../Routing/HomeRouting';
import TransactionRouting from '../Routing/TransactionRouting';

const Stack = createNativeStackNavigator();
const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="SplashScreen">
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        <Stack.Screen name="MainRouting" component={MainRouting} />
        <Stack.Screen name="HomeRouting" component={HomeRouting} />
        <Stack.Screen
          name="TransactionRouting"
          component={TransactionRouting}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routing;

