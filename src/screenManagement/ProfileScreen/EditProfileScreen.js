import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  StyleSheet,
} from 'react-native';
import ProfileRouting from '../Routing/ProfileRouting';
import AsyncStorage from '@react-native-async-storage/async-storage';

const EditProfileScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        {/* Form Pesan ====================================================================================== */}
        <View
          style={{
            flexDirection: 'row',
            padding: 25,
            backgroundColor: '#DCDCDC10',
            bottom: 10,
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              source={require('../../assets/Back.png')}
              style={{
                width: 35,
                height: 25,
                marginTop: 5,
                marginEnd: 5,
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              paddingStart: 7,
            }}>
            <Text
              style={{
                fontSize: 28,
                fontWeight: '700',
                color: 'black',
              }}>
              Edit Profile
            </Text>
          </View>
        </View>
        {/* Foto dan Biodata ==================================================================================== */}
        <View style={{backgroundColor: 'white', padding: 10}}>
          <Image
            style={{
              width: 95,
              height: 95,
              borderRadius: 10,
              marginTop: 10,
              alignSelf: 'center',
            }}
            source={require('../../assets/Wasis.jpg')}
          />
          <TouchableOpacity>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                paddingTop: 10,
              }}>
              <Image
                style={{
                  width: 20,
                  height: 20,
                }}
                source={require('../../assets/Pensil.png')}
              />
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: '600',
                  color: '#3A4BE0',
                  paddingStart: 10,
                }}>
                Edit Foto
              </Text>
              <View></View>
            </View>
          </TouchableOpacity>
          <View
            style={{
              backgroundColor: 'white',
              paddingHorizontal: 20,
              paddingTop: 10,
              marginTop: 20,
            }}>
            <Text style={{color: '#BB2427', fontWeight: 'bold'}}>Nama</Text>
            <TextInput
              placeholder="Wasis Setia Nugroho"
              style={{
                marginTop: 10,
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="default"
            />
            <Text style={{color: '#BB2427', fontWeight: 'bold', marginTop: 15}}>
              Email
            </Text>
            <TextInput
              placeholder="wasissetianugroho@gmail.com"
              secureTextEntry={true}
              style={{
                marginTop: 10,
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="email-address"
            />
            <Text style={{color: '#BB2427', fontWeight: 'bold', marginTop: 15}}>
              No hp
            </Text>
            <TextInput
              placeholder="081393530013"
              style={{
                marginTop: 10,
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="numeric"
            />
          </View>

          {/* Masukkan Keranjang =========================================================================================== */}
          <TouchableOpacity
            onPress={() => navigation.navigate('profileScreen')}
            style={{
              width: '100%',
              backgroundColor: '#BB2427',
              borderRadius: 8,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 240,
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Simpan
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default EditProfileScreen;