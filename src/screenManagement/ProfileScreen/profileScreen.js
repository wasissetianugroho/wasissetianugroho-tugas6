import {
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React, {Component} from 'react';
  import {styles} from '../../assets/style/HomeStyle';
  import {useDispatch, useSelector} from 'react-redux';
  
  const ProfileScreen = ({navigation}) => {
    const {session} = useSelector(state => state.session);
    const dispatch = useDispatch();
    return (
      <View style={styles.safeAreaContent}>
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
          }}>
          {/* PROFILE PICTURE ==================================================================== */}
          <View
            style={{
              backgroundColor: 'white',
              alignItems: 'center',
              paddingTop: 52,
              paddingBottom: 26,
            }}>
            <Image
              style={localStyle.profilePict}
              source={require('../../assets/Wasis.jpg')}
            />
            <Text style={[localStyle.profleName, {marginTop: 8}]}>
              Wasis Setia Nugroho</Text>
            <Text style={[localStyle.profleEmail]}>wasissetianugroho@gmail.com</Text>
  
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('EditProfileScreen')}
              style={{
                backgroundColor: '#F6F8FF',
                borderRadius: 16,
                paddingHorizontal: 20,
                paddingVertical: 8,
                marginTop: 24,
              }}>
              <Text style={{color: '#050152', fontSize: 10, fontWeight: '900'}}>
                EDIT
              </Text>
            </TouchableOpacity>
          </View>
  
          {/* MENU ======================================================================== */}
          <View style={{padding: 16}}>
            <TouchableOpacity style={localStyle.menuOption}>
              <Text style={localStyle.textMenu}>About</Text>
            </TouchableOpacity>
            <TouchableOpacity style={localStyle.menuOption}>
              <Text style={localStyle.textMenu}>Terms & Condition</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={localStyle.menuOption}
              onPress={() => navigation.navigate('FAQScreen')}>
              <Text style={localStyle.textMenu}>FAQ</Text>
            </TouchableOpacity>
            <TouchableOpacity style={localStyle.menuOption}>
              <Text style={localStyle.textMenu}>History</Text>
            </TouchableOpacity>
            <TouchableOpacity style={localStyle.menuOption}>
              <Text style={localStyle.textMenu}>Setting</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                dispatch({type: 'SESSION_DELETE_DATA'});
                navigation.replace('LoginScreen');
              }}
              style={[
                localStyle.menuOption,
                {flexDirection: 'row', marginTop: 19, justifyContent: 'center'},
              ]}>
              <Image
                style={{width: 24, height: 24}}
                source={require('../../assets/log_out.png')}
              />
              <Text
                style={[localStyle.textMenu, {color: '#EA3D3D', marginLeft: 2}]}>
                Logout
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  };
  
  const localStyle = StyleSheet.create({
    profilePict: {
      width: 95,
      height: 95,
    },
    profleName: {
      fontSize: 20,
      fontWeight: '700',
      color: '#050152',
    },
    profleEmail: {
      fontSize: 10,
      fontWeight: '500',
      color: '#A8A8A8',
    },
    menuOption: {
      backgroundColor: 'white',
      paddingVertical: 18,
    },
    textMenu: {
      marginLeft: 80,
      fontSize: 16,
      fontWeight: '500',
      color: '#000000',
    },
  });
  
  export default ProfileScreen;  