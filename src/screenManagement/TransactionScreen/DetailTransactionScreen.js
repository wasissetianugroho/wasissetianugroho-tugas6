import {
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React from 'react';
  import {textStyle} from '../../assets/style/TextStyle';
  
  const DetailTransactionScreen = ({navigation, route}) => {
    console.log('detail transaction params: ', route.params);
    const data = route.params;
    return (
      <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
          }}>
          {/* KODE ====================================================== */}
          <View
            style={{
              backgroundColor: 'white',
              paddingVertical: 24,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={[textStyle.size15BlackNormal, {color: '#BDBDBD'}]}>
              {data.date}
            </Text>
            <Text style={[textStyle.size36BlackBold, {marginTop: 54}]}>
              C{data.id_trans.substring(0, 6)}
            </Text>
            <Text style={[textStyle.size15BlackNormal, {marginTop: 8}]}>
              Kode Reservasi
            </Text>
            <Text
              style={[
                textStyle.size15BlackNormal,
                {marginTop: 42, color: '#6F6F6F'},
              ]}>
              Sebutkan Kode Reservasi saat tiba di outlet{' '}
            </Text>
          </View>
          {/* BARANG ==================================================================== */}
          <View style={{marginTop: 12, marginHorizontal: 12}}>
            <Text style={textStyle.size15BlackNormal}>Barang</Text>
            <View style={localStyle.cardView}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={require('../../assets/SepatuNB.png')}
                  style={localStyle.cardImage}
                />
                <View
                  style={{marginHorizontal: 13, justifyContent: 'space-evenly'}}>
                  <Text style={textStyle.size15BlackBold}>
                    {data.product.merek} - {data.product.warna} -{' '}
                    {data.product.ukuran}
                  </Text>
                  <Text style={[textStyle.size15BlackNormal, {color: '#737373'}]}>
                    {data.product.service.map(value => value.key).join(', ')}
                  </Text>
                  <Text style={[textStyle.size15BlackNormal, {color: '#737373'}]}>
                    Note : {data.product.note}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          {/* STATUS PESANAN ================================================================= */}
          <View style={{marginTop: 36, marginHorizontal: 12}}>
            <Text style={textStyle.size15BlackNormal}>Status Pesanan</Text>
            <TouchableOpacity
              onPress={() =>{
                if (!data.paid) {navigation.navigate('CheckOutScreen', data);}}}
              style={localStyle.cardView}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'flex-start',
                }}>
                <Image
                  style={{
                    width: 14,
                    height: 14,
                    tintColor: data.paid ? 'green' : 'red',
                  }}
                  source={require('../../assets/TitikMerah.png')}
                />
                <View style={{marginHorizontal: 13}}>
                  <Text style={textStyle.size15BlackBold}>
                    {data.paid ? 'Telah Dibayar' : 'Telah Reservasi'}
                  </Text>
                  <Text style={[textStyle.size15BlackNormal, {color: '#737373'}]}>
                    {data.date}{' '}
                  </Text>
                </View>
                <Text style={{marginLeft: 'auto'}}>09:00</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  };
  
  const localStyle = StyleSheet.create({
    cardView: {
      backgroundColor: 'white',
      paddingHorizontal: 14,
      paddingVertical: 24,
      borderRadius: 8,
      elevation: 4,
      marginTop: 16,
    },
    cardImage: {
      width: 84,
      height: 84,
      resizeMode: 'contain',
      borderRadius: 8,
    },
  });
  
  export default DetailTransactionScreen;
  
  