import {
    FlatList,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React from 'react';
  import {textStyle} from '../../assets/style/TextStyle';
  import {useSelector} from 'react-redux';
  
  const TransactionScreen = ({navigation}) => {
    const {trans} = useSelector(state => state.Transaction);
    return (
      <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
          }}>
          <FlatList
            data={trans}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('TransactionRouting', {
                    screen: 'DetailTransactionScreen',
                    params: item,
                  })
                }
                style={[localStyle.cardView]}>
                <Text style={[textStyle.size15BlackNormal, {color: '#BDBDBD'}]}>
                  {item.date}
                </Text>
                <Text style={[textStyle.size15BlackBold, {marginTop: 13}]}>
                  {item.product.merek} - {item.product.warna} -{' '}
                  {item.product.ukuran}
                </Text>
                <Text style={[textStyle.size15BlackNormal]}>
                  {item.product.service.map(text => text.key).join(', ')}
                </Text>
                <View style={localStyle.rowContentBetween}>
                  <Text style={[textStyle.size15BlackNormal]}>
                    Kode Reservasi :{' '}
                    <Text style={[textStyle.size13BlackBold]}>
                      C{item.id_trans.substring(0, 6)}
                    </Text>
                  </Text>
                  <Text
                    style={[
                      localStyle.transactionStatus,
                      {backgroundColor: item.paid ? 'green' : '#F29C1F29'},
                    ]}>
                    {item.paid ? 'Paid' : 'Reserved'}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </ScrollView>
      </View>
    );
  };
  
  const localStyle = StyleSheet.create({
    cardView: {
      backgroundColor: 'white',
      margin: 12,
      borderRadius: 8,
      padding: 16,
      elevation: 4,
    },
    rowContentBetween: {
      flexDirection: 'row',
      marginTop: 13,
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    transactionStatus: {
      backgroundColor: '#F29C1F29',
      color: '#FFC107',
      paddingHorizontal: 10,
      paddingVertical: 4,
      borderRadius: 24,
    },
  });
  export default TransactionScreen;  